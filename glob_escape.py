import glob

class FilterModule(object):
    def filters(self):
        return {"glob_escape": self.glob_escape}

    def glob_escape(self, pathname):
        return glob.escape(pathname)
